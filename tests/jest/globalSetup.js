const server = require('../../examples-graphql/testGraphql/graphql');
const config = require('../config');
module.exports = async () => {
  console.log(config.port);
  global.server = await server.start({ port: config.port}, ({port}) => {
    console.log(port);
  });
};