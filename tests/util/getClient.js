require('cross-fetch/polyfill');
const config = require('../config');

const ApolloBoost = require('apollo-boost').default;
const client = new ApolloBoost({
  uri: `http://localhost:${config.port}`,
});

const getQuery = async (query) => {
  const response = await client.query({ query, fetchPolicy: 'network-only'   });
  return response;
}

module.exports = {
  getQuery,
};