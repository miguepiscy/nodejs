const {getFirstName, sum} = require('../models/user');
// const APIError = require('../helpers/APIError');
const gpl = require('graphql-tag');
const {  getQuery } = require('./util/getClient');
beforeEach(() => {
  console.log('before each fuera');
})

it('hola', () => {
  expect(3).toBe(3);
});

describe('graphql', () => {
  it('query user', async () => {
    const getUser = gpl`
      query {
        getUser {
          name
        }
      }
    `;
    const response = await getQuery(getUser);
   expect(response.data.getUser.name).toBe('Maria');
  })
});

describe('getFirstName',() => {
  let name;
  const obj = { hola: 'Juan'};
  let testObj;
  beforeAll(() => {
    name = 'Miguel Diaz';
  })

  beforeEach(() => {
    console.log('beforeEach');
    testObj = Object.assign({}, obj);
  })

  it('Should return firstname', () => {
    const firstName = getFirstName(name);
    expect(firstName).toBe('Miguel');
    expect(firstName.length).toBe(6);
  })

  it('Should throw if no name', () => {
    expect(() => {
      getFirstName('');
    }).toThrow();
    // try {
    // } catch (error) {
    //   expect(error instanceof APIError).toBe(true);
    // }
    // obj.a.b.c => lodas.get(objt, "a.b.c") =>  obj.a?.b.c
  });

  it.only('mock', () => {
    const mockFn = jest.fn();
    const result = sum(2, 3, mockFn);
    const calls = mockFn.mock.calls;
    expect(result).toBe(5);
    expect(mockFn).toBeCalled();
    expect(calls[0][0]).toBe(5);
  })
} )

