
const getFirstName = (fullName) => {
  if (!fullName) {
    throw new Error('NO name');
  }
  const partsOfName = fullName.split(' ');
  const firstName = partsOfName[0];
  return firstName;
};

const sum = (a, b, callack) => {
  const result = a + b;
  callack(result, 'otro', 'otro2');
  callack(result, 'otro3', 'otro4');
  return result;
}


module.exports = {
  getFirstName,
  sum,
}