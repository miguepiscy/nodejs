function throwErrorIfNotExist(obj) {
  if (!obj) {
    const err = new APIError('Not Exists', httpStatus.NOT_FOUND);
    throw err;
  }
  return obj;
}

function convertStringToJson(str) {
  return JSON.parse(str);
}

module.exports = {
  throwErrorIfNotExist,
  convertStringToJson,
}