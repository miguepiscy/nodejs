const router = require('express').Router();
const APIError = require('../helpers/APIError');
const httpStatus = require('http-status');
const config = require('../config');
const helper = require('../helpers/helper');
const client = require('../lib/redis'); 

function getUser(id) {
  return client.get(`user:${id}`)
    .then(helper.convertStringToJson)
    .then(helper.throwErrorIfNotExist);
}

router.get('/:id', (req, res, next) => {
  const userId = parseInt(req.params.id);
  return getUser(userId)
    .then((user) => res.json(user))
    .catch(next);
})

router.get('/names/:name', (req, res, next) => {
   const name = req.params.name;
   return client.getAllOfSet(`name:${name}`)
    .then(res.json.bind(res))
    .catch(next);
} )

router.post('/:id', (req, res, next) => {
  const userId = req.params.id;
  const jsonToString = JSON.stringify(req.body);
  client.set(`user:${userId}`, jsonToString);
  client.addToSet(`name:${req.body.name}`, userId )
  res.json({ok: 'ok'});
})

module.exports = router;