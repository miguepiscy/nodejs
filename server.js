const APIError = require('./helpers/APIError');
const express = require('express');
const httpStatus = require('http-status');
const lodash = require('lodash');
const PORT = process.env.PORT || 3500;
const userController = require('./controllers/userController')
const grapqhlServer = require('./graphql');
const libredis = require('./lib/redis');

const app = express();

app.use(express.json());

const URLGRAPHQL = '/graphql';
app.use(URLGRAPHQL, middlewareCache)
grapqhlServer.applyMiddleware({ app, path: URLGRAPHQL});

app.use('/api/user', userController);


app.use((req, res, next) => {
  const  err = new APIError('API NOT FOUND', httpStatus.NOT_FOUND);
  next(err);
})

app.use((err, req, res, next) => {
  if (err instanceof APIError) {
    return next(err);
  }
  const error = new APIError('?', httpStatus.INTERNAL_SERVER_ERROR);
  return next(error);
})

app.use((err, req, res, next) => {
  res.status(err.status).json({
    message: err.message,
  })
});

app.listen(PORT);


function middlewareCache(req, res, next) {
  console.log('middlewarre', process.env.NODE_ENV);
  if (process.env.NODE_ENV === 'development' && !process.env.CACHE) {
    return next();
  }

  if (!req.body.query) {
    return next();
  }
  // let expire = 300000000;
  // if (lodash.startsWith(query, '{hello')) {
  //   expire = 3000;
  // }
  const query = req.body.query.replace(/\s/g, '');
  return libredis.get(query)
    .then((reply) => {
      if(reply) {
        res.write(reply);
        res.end();
      } else {
        res.on('finish', () => {
          libredis.set(query, res.gplResponse);
        });
        res.originalWrite = res.write;
        res.write = function (gplResponse) {
          res.gplResponse = gplResponse;
          // return res.originalWrite.apply(this, arguments);
          return res.originalWrite(...arguments);
        }
        next();
      }
    }).catch(() => next());
}

// Solucion mala
// function middlewareGetCache(req, res, next) {
//   if (estoyEnCache) {
//     res.json(resultado);
//   }
//   req.cacheable = true;
// }

// function resolve() {
//   En vez de res.json()
//   res.result;
//   next();
// }

// function middewareWrite() {
//   caches(res.result);
//   res.json(res.result);
// }

