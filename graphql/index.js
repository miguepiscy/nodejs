const { ApolloServer, gql } = require('apollo-server-express');
const libBull = require('../lib/bull');
const config = require('../config');
const shortId = require('shortid');

const queue = libBull.create(config.queue.mails);

const typeDefs = gql`
  type Query {
    hello: String
  }
  type Mutation {
    sendMail(mail: String): String
  }
`;

const resolvers = {
  Query: {
    hello: () => {
      console.log('entro en grpahql');
      return 'Hello world!';
     } ,
  },
  Mutation: {
    sendMail(_, { mail }) {
      const id = shortId.generate();
      libBull.add(queue, { mail, id });
      return id;
    }
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

module.exports = server;