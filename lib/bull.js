const Queue = require('bull');
const { redisHost, redisPort } = require('../config');
// redis//localhost:16379
function create(name) {
  const queue = new Queue(name,
      { redis: {port: redisPort, host: redisHost}});
  return queue;
}

function consume(queue, callback) {
  queue.process(callback);
}

function add(queue, value) {
  queue.add(value);
}

module.exports = {
  create,
  consume,
  add,
}