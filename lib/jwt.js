const jwt = require('jsonwebtoken');
const jwtSecret = 'password';
const APIError = require('../helpers/APIError');
const httpStatus = require('http-status');

function sign({ id, name }) {
  const token = jwt.sign({
    time: new Date().getTime(),
    user: { id, name },
  }, jwtSecret)

  return token;
}

function getUserSign(token) {
  let user;
  try {
    const decoded = jwt.verify(token, jwtSecret);
    user = decoded.user;
  } catch(err) {
    throw new APIError('No autorizado',  httpStatus.UNAUTHORIZED);
  }
  return user;
}

module.exports = {
  sign,
  getUserSign,
}