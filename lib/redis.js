const config = require('../config');
const redis = require('redis');
const redisClient = redis.createClient(config.redisPort, config.redisHost);
const util = require('util');

function get(key) {
  const get = util.promisify(redisClient.get)
    .bind(redisClient);
  return get(key);
}

function getJson(key) {
  return get(key)
    .then(JSON.parse);
}

function set(key, value) {
  redisClient.set(key, value);
}

function setJson(key, value) {
  set(key, JSON.stringify(value));
}

function keys(expreg) {
  const keys = util.promisify(redisClient.keys).bind(redisClient);
  return keys(expreg)
}

function addToSet(key, value) {
  const addToSet = util.promisify(redisClient.sadd).bind(redisClient);
  return addToSet(key, value);

}

function getAllOfSet(key) {
  const get = util.promisify(redisClient.smembers).bind(redisClient);
  return get(key);

}

module.exports = {
  get,
  set,
  addToSet,
  getAllOfSet,
  getJson,
  setJson,
  keys,
}