const libBull = require('../lib/bull');
const config = require('../config');
const queue = libBull.create(config.queue.hola);


// function addPostsToUser(user) {
//   ....
// }

// return getUser(1)
//   .then(addPostsToUser)
//   .then((user) => {
//     return getLikes()
//       .then((posts) => {
//         user.posts = posts;
//         return user;
//       })
//   });



libBull.consume(queue, (job, done) => {
  console.log(job.data);
  job.progress(42);
  queue.getCompleted()
    .then((data) => console.log('Completed', data.length))
    .then(() => queue.getWaiting())
    .then((data) => console.log('Waiting', data.length))
    .then(() => queue.getActive())
    .then((data) => console.log('Active', data.length))
    .then(() => queue.getDelayed())
    .then((data) => console.log('Delayed', data.length))
    .then(() => queue.getFailed())
    .then((data) => console.log('Failed', data.length))
    .then(() => {
      setTimeout(() => {
        done(null, job.data);
      }, 2000);
    })
  });