const libBull = require('../lib/bull');
const config = require('../config');
const queue = libBull.create(config.queue.hola);

setInterval(() => {
  libBull.add(queue, { hola: 'hola mundo'});
}, 500);