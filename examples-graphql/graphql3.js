const graphql = require('graphql-yoga');
const GraphQLServer = graphql.GraphQLServer;
const shortid = require('shortid');
const libredis = require('../lib/redis');
const lodash = require('lodash');

const objeto = { a: 'hola', b: { c: 'hola' } };
const objeto2 = { a: 'mundo' };

//evitar undefined.algo
console.log(lodash.get(objeto, 'a'));
console.log(lodash.get(objeto, 'b.c'));
console.log(lodash.get(objeto2, 'a.b.c', 'alternativa'));
//  user = { id, name }
// mutation(name) => user
// query(id) => user

// query users => [{id:12323, name:'maria'}, {id: 1233, name:'maria'}]


//  interface también se puede con => union Search = User | Teacher
// query => search() : Search
const typeDefs = `
  interface Search {
    id: !ID
  }
  type User implements Search {
    id: ID!
    name: String
  }

  type Teacher implements Search {
    id: ID!
    name: String
  }

  type Query {
    getUser(id: ID!): User
    users : [User]
  }
  type Mutation {
    createUser(name: String!, surname: String): User
  }
`;

const resolvers = {
  Query: {
    getUser(_, { id }, ctx, info) {
      return libredis.getJson(`user:${id}`);
      // return libredis.get(`user:${id}`).then(JSON.parse);
    },
    users: async () => {
      const keys = await libredis.keys('user:*');
      const promises = [];
      keys.forEach(key => {
        promises.push(libredis.getJson(key));
      });
      const users = await Promise.all(promises);
      return users;
    }
  },
  Mutation: {
    createUser(_, data, ctx, info) {
      const id = shortid.generate();
      const user = Object.assign({ id }, data);
      libredis.setJson(`user:${id}`, user);
      return user;
    }
  }
}

const server = new GraphQLServer({
  typeDefs,
  resolvers,
})

server.start(() => console.log('start'))