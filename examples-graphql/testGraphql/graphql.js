const graphql = require('graphql-yoga');
const GraphQLServer = graphql.GraphQLServer;

const typeDefs = `
  type User {
    name: String
  }
  type Query {
    _empty: String
    getUser: User
  }
`;

const resolvers = {
  Query: {
    getUser() {
      return { name: 'Maria'};
    },
  }
};

const server = new GraphQLServer({
  typeDefs,
  resolvers,
})

module.exports = server;