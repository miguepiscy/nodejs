// import graphql from 'graphql-yoga';
// import {  GraphQLServer } from 'graphql-yoga';
// import { get } from 'lodash'; pesa más
// import get from 'lodash/get'; pesa poco

const graphql = require('graphql-yoga');
const GraphQLServer = graphql.GraphQLServer;
const { sign, getUserSign } = require('../lib/jwt');

const typeDefs = `
  type Direction {
    street: String
    cp: Int
  }

  type User {
    id: ID!
    name: String
    username: String
    direction: Direction
  }

  type Admin {
    id: ID,
    name: String
    direction: Direction
  }
  type Query {
    hello(name: String!): User!
    admin: Admin,
    profile(token: String): User,
  }

  type Token {
    token: String
  }

  type Mutation {
    createUser(name: String): Token
  }
`;

const user = { id: 1, name: 'Maria'};
const directions = [
  { userId:1, street: 'avd'},
];

function getPostalCode() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(28045);
    }, 3000);
  })
}

function getDirection(id) {
  const direction = directions.find((direction) => {
    return direction.userId === id;
  });

  return direction;
}

const db = [];

const resolvers = {
  Query: {
    hello(_, { name }, ctx) {
      console.log(ctx.request);
      ctx.requireAuth();
      const userResult = Object.assign({}, user);
      return userResult;
    },
    admin() {
      return {
        id: 2,
        name: 'Admin',
        direction: {
          street: 'calle ..',
          cp: 23066,
        }
      }
    },
    profile(_, { token }) {
      return getUserSign(token);
    }
  },
  Mutation: {
    createUser(_, user ) {
      user.id = 1;
      const token = sign(user);
      return {
        token: token,
      }
    }
  },
  User: {
    direction(parent) {
      return getDirection(parent.id);
    }
  },
  Direction: {
    cp(parent) {
      if (parent.cp) {
        return parent.cp;
      }
      return getPostalCode();
    }
  }
}

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context(request) {
    return {
      requireAuth() {
        return console.log('auth');
      },
    };
  }
})

server.start({}, ({ port }) =>
  console.log(
    `Server started, listening on port ${port} for incoming requests.`,
  ),
)
