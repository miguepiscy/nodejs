module.exports = {
  redisHost: '127.0.0.1',
  redisPort: 16379,
  queue: {
    hola: 'hola',
    mails: 'mails',
  },
};